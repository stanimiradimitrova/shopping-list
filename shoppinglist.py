from flask import Flask, render_template, request

def get_listitems(): 
    listitems = { "Miscellaneous" : [("Antihistamines", 6.00), ("Cards", 5.00), ("Newspaper", 2.00), ("Wrapping paper", 4.00)], "Fruit and vegetables" : [("Apples", 2.00), ("Avocadoes", 4.00), ("Bananas", 3.00), ("Berries", 10.00), ("Broccoli", 3.00), ("Cabbage", 3.00), ("Carrots", 2.00), ("Cauliflower", 4.00), ("Celery", 4.00), ("Cherries", 6.00), ("Chillis", 10.00), ("Corn on cob", 3.00), ("Courgettes", 3.00), ("Cucumber", 4.00), ("Fresh herbs", 3.00), ("Garlic", 8.00), ("Ginger", 7.00), ("Grapes", 4.00), ("Lemons", 3.00), ("Lettuce", 3.00), ("Limes", 4.00), ("Mange tout", 5.00), ("Melons", 3.00), ("Mushrooms", 6.00), ("Nectarines", 4.00), ("Onions", 2.00), ("Oranges", 3.00), ("Peaches", 3.00), ("Pears", 2.00), ("Peppers", 4.00), ("Plums", 5.00), ("Potato", 2.00), ("Raspberries", 10.00), ("Salad leaves", 3.00), ("Shallots", 3.00), ("Spinach", 5.00), ("Strawberries", 10.00), ("Sugar snaps", 5.00), ("Sweet potato", 7.00), ("Tomatoes", 3.00), ("Watermelon", 4.00)], "Meat" : [("Chicken", 6.00), ("Mince", 10.00)], "Bakery" : [("Bagels", 3.00), ("Bread for kids", 3.00), ("Bread for parents", 2.00), ("Sliced bread", 2.00), ("Wraps", 3.00)], "Lunch" : [("Hummus", 4.00), ("Oatcakes", 2.00), ("Pitta bread", 3.00), ("Quorn eggs", 3.00)], "Dairy" : [("Butter (cooking)", 4.00), ("Butter (low fat)", 5.00), ("Cheese (block/grated)", 10.00), ("Cottage cheese", 4.00), ("Cream cheese", 4.00), ("Creme fraiche", 3.00), ("Eggs", 1.00), ("Milk", 3.00), ("Yogurt", 2.00)], "Ingredients and condiments" : [("Brown sugar", 4.00), ("Cashew nuts", 6.00), ("Dried fruit", 5.00), ("Flour", 2.00), ("Honey", 10.00), ("Jam", 5.00), ("Ketchup", 3.00), ("Lea and Perrins", 3.00), ("Lemon juice", 2.00), ("Mayonnaise", 3.00), ("Mustard", 4.00), ("Olive oil", 10.00), ("Pasta", 2.00), ("Pepper", 1.00), ("Rice", 3.00), ("Salad dressing", 4.00), ("Salt", 1.00), ("Soy sauce", 5.00), ("Sugar", 3.00), ("Vegetable oil", 3.00)], "Tins" : [("Baked beans", 2.00), ("Butter beans", 3.00), ("Chickpeas", 5.00), ("Kidney beans", 5.00)], "Breakfast" : [("Muesli", 3.00), ("Peanut butter", 6.00), ("Porridge", 2.00), ("Weetabix", 3.00)], "Desserts" : [("Ice cream", 2.00), ("Jelly", 1.00)], "Household" : [("Batteries", 1.00), ("Bird food", 2.00), ("Bleach", 2.00), ("Cling film", 3.00), ("Cotton buds", 2.00), ("Dental floss", 3.00), ("Deodorant (Neena)", 4.00), ("Deodorant (Paul)", 3.00), ("Dishwasher tablets", 2.00), ("Dog food", 4.00), ("Freezer/sandwich bags", 5.00), ("Furniture polish", 4.00), ("Hand wash dispensers", 3.00), ("Kitchen foil", 2.00), ("Light bulbs", 2.00), ("Mouthwash", 7.00), ("Paper towels", 3.00), ("Plasters", 2.00), ("Razors (Neena)", 2.00), ("Razors (Paul)", 3.00), ("Rubbish bags", 2.00), ("Sandwich bags", 1.00), ("Shampoo (Paul)", 7.00), ("Shampoo for kids", 8.00), ("Shaving foam", 3.00), ("Soap", 2.00), ("Sponges /scrubbers", 1.00), ("Tissues", 4.00), ("Toilet paper", 4.00), ("Toothbrushes", 5.00), ("Toothpaste", 5.00), ("Washing powder", 4.00), ("Washing up liquid", 3.00)], "Frozen" : [("Fish fingers", 5.00), ("Frozen pizzas", 8.00), ("Frozen vegetables", 4.00)], "Snacks" : [("Biscuits", 3.00), ("Crackers", 3.00), ("Crisps", 3.00), ("Nuts", 5.00), ("Sweets", 4.00)], "Drinks" : [("Beer", 3.00), ("Bottled water", 4.00), ("Chai", 2.00), ("Cocoa", 2.00), ("Coffee", 5.00), ("Fruit juice", 3.00), ("Herbal teas", 2.00), ("Rooibos", 3.00), ("Tea", 2.00), ("Vimto", 4.00), ("Wine", 10.00)]}
    return listitems

saved_info = []

app = Flask(__name__, template_folder = 'templates')

@app.route('/')
def index():
    return render_template('index.html', 
            listitems = get_listitems(), discount=0, currency="USD")

@app.route('/save_info', methods=['POST'])
def save_info_route():
    if request.method == 'POST':
        user = request.form['user']
        time_range = request.form['time-range']
        average = request.form['average']
        saved_info.append({'user': user, 'time_range': time_range, 'average': average})
        return "Information saved successfully!"

@app.route('/max_average', methods=['GET'])
def max_average():
    if request.method == 'GET':
        time_range = request.args.get('time_range')
        max_avg = get_max_average(time_range)
        return f"Maximum average for time range {time_range}: {max_avg}"

def get_max_average(time_range):
    averages_for_time_range = [info['average'] for info in saved_info if info['time_range'] == time_range]
    if averages_for_time_range:
        return max(averages_for_time_range)
    else:
        return "No data available for the chosen time range."


@app.route('/about/')
def about():
    return render_template('about.html')

if __name__ == '__main__':
    app.run(host='0.0.0.0')



    

